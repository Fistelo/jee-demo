package com.fistelo.uni.view.config;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.fistelo.uni.persistence.entity.User;
import com.fistelo.uni.service.AccountService;
import lombok.Getter;
import lombok.Setter;

@ViewScoped
@Named
public class EditUser implements Serializable {

  private final AccountService accountService;

  @Inject
  public EditUser(final AccountService accountService) {
    this.accountService = accountService;
  }

  @Getter
  @Setter
  private User user = new User();

  public String edit (){
    accountService.editUser(user);
    return "dashboard?faces-redirect=true";
  }

}