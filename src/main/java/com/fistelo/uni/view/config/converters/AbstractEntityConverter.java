package com.fistelo.uni.view.config.converters;


import java.util.function.BiFunction;
import java.util.function.Function;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.fistelo.uni.service.AccountService;

abstract class AbstractEntityConverter<T> implements Converter<T> {

  @Inject
  AccountService accountService;

  private BiFunction<AccountService, Long, T> retrieveFunction;

  private Function<T, Long> idExtractor;

  AbstractEntityConverter(Function<T, Long> idExtractor, BiFunction<AccountService, Long, T> retrieveFunction) {
    this.retrieveFunction = retrieveFunction;
    this.idExtractor = idExtractor;
  }

  @Override
  public T getAsObject(FacesContext context, UIComponent component, String value) {
    T entity = retrieveFunction.apply(accountService, Long.parseLong(value));

    if (entity == null) {
      context.getExternalContext().setResponseStatus(HttpServletResponse.SC_NOT_FOUND);
      context.responseComplete();
    }

    return entity;
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, T value) {
    Long id = idExtractor.apply(value);
    return id != null ? id.toString() : null;
  }
}