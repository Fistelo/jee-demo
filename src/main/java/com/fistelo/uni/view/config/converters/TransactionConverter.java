package com.fistelo.uni.view.config.converters;

import javax.enterprise.context.Dependent;
import javax.faces.convert.FacesConverter;

import com.fistelo.uni.persistence.entity.Transaction;
import com.fistelo.uni.service.AccountService;

@FacesConverter(forClass = Transaction.class, managed = true)
@Dependent
public class TransactionConverter extends AbstractEntityConverter<Transaction>{
  public TransactionConverter() {
    super(Transaction::getId, AccountService::findTransaction);
  }
}
