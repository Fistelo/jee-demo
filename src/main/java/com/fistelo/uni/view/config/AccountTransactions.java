package com.fistelo.uni.view.config;

import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.fistelo.uni.persistence.entity.Account;
import com.fistelo.uni.persistence.entity.Transaction;
import com.fistelo.uni.service.AccountService;

@Named
@RequestScoped
public class AccountTransactions implements Serializable {
  /*TransactionService when db will be up*/
  private final AccountService accountService;

  @Inject
  public AccountTransactions(final AccountService accountService) {
    this.accountService = accountService;
  }

  public Collection<Transaction> getTransactions(final long account){
    return accountService.getTransactions(account);
  }

  public String removeTransaction(final Transaction transaction){
    accountService.deleteTransaction(transaction);
    return "/account_history.xhtml?faces-redirect=true&account=" + transaction.getAccount().getId();
  }

  public Account getAccount(final long id){
    final Optional<Account> account = accountService.getAllAccounts().stream()
        .filter(acc -> acc.getId().equals(id))
        .findFirst();
    return account.isPresent() ? account.get() : null;
  }
}
