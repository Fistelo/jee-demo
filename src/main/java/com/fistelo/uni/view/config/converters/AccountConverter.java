package com.fistelo.uni.view.config.converters;

import javax.enterprise.context.Dependent;
import javax.faces.convert.FacesConverter;

import com.fistelo.uni.persistence.entity.Account;
import com.fistelo.uni.service.AccountService;

@FacesConverter(forClass = Account.class, managed = true)
@Dependent
public class AccountConverter extends AbstractEntityConverter<Account> {
  public AccountConverter() {
    super(Account::getId, AccountService::findAccount);
  }
}
