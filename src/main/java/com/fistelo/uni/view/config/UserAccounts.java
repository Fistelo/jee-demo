package com.fistelo.uni.view.config;

import java.io.Serializable;
import java.util.Collection;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.fistelo.uni.persistence.entity.Account;
import com.fistelo.uni.service.AccountService;

@Named
@RequestScoped
public class UserAccounts implements Serializable {

  private final AccountService accountService;

  @Inject
  public UserAccounts(final AccountService accountService) {
    this.accountService = accountService;
  }

  public Collection<Account> getAccounts(){
    return accountService.getAllAccounts();
  }

  public String showHistory(){
    return "account_history.xhtml";
  }
}
