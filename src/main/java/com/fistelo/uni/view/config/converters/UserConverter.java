package com.fistelo.uni.view.config.converters;

import javax.enterprise.context.Dependent;
import javax.faces.convert.FacesConverter;

import com.fistelo.uni.persistence.entity.User;
import com.fistelo.uni.service.AccountService;

@FacesConverter(forClass = User.class, managed = true)
@Dependent
public class UserConverter extends AbstractEntityConverter<User>{
  public UserConverter() {
    super(User::getId, AccountService::findUser);
  }
}