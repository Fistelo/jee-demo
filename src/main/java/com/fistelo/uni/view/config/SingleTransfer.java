package com.fistelo.uni.view.config;

import static com.fistelo.uni.persistence.entity.Action.LOSS;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.fistelo.uni.persistence.entity.Account;
import com.fistelo.uni.persistence.entity.Transaction;
import com.fistelo.uni.service.AccountService;
import lombok.Getter;
import lombok.Setter;

@ViewScoped
@Named
public class SingleTransfer implements Serializable {

  private final AccountService accountService;

  @Inject
  public SingleTransfer(final AccountService accountService) {
    this.accountService = accountService;
  }

  @Getter
  @Setter
  private Transaction transaction = new Transaction();

  public List<Account> getAvailableAccounts(){
    return accountService.getAllAccounts();
  }

  public String transferMoney(){
    transaction.setAction(LOSS);
    accountService.transferMoney(transaction);
    return "dashboard?faces-redirect=true";
  }

}
