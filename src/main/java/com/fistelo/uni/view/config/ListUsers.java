package com.fistelo.uni.view.config;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.fistelo.uni.persistence.entity.User;
import com.fistelo.uni.service.AccountService;

@ViewScoped
@Named
public class ListUsers implements Serializable {
  private final AccountService accountService;

  @Inject
  public ListUsers(final AccountService accountService) {
    this.accountService = accountService;
  }

  public User getUser(){
    return accountService.findUser(0L);
  }

}
