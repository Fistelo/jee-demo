package com.fistelo.uni.view.config;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.fistelo.uni.persistence.entity.Account;
import com.fistelo.uni.service.AccountService;
import lombok.Getter;
import lombok.Setter;

@ViewScoped
@Named
public class EditAccount implements Serializable {

  private final AccountService accountService;

  @Inject
  public EditAccount(final AccountService accountService) {
    this.accountService = accountService;
  }

  @Getter
  @Setter
  private Account account = new Account();

  public String edit() {
    accountService.editAccount(account);
    return "dashboard?faces-redirect=true";
  }
}