package com.fistelo.uni.service;

import static com.fistelo.uni.persistence.entity.Action.INCOME;
import static com.fistelo.uni.persistence.entity.Action.LOSS;
import static java.util.Arrays.asList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.fistelo.uni.persistence.dao.AccountJpaDao;
import com.fistelo.uni.persistence.entity.Account;
import com.fistelo.uni.persistence.entity.Transaction;
import com.fistelo.uni.persistence.entity.User;

@ApplicationScoped
public class AccountService {

  private final AccountJpaDao accountJpaDao;
  private List<Account> accounts = new ArrayList<>();
  private List<User> users = new ArrayList<>();
  private List<Transaction> transactions = new ArrayList<>();

  /* Delete after introducing database */
  @PostConstruct
  public void prepareData() {
    final User user1 = new User(0L, "Chuck", "Norris", "chuck.norris@gmail.com", "668936752");
    final Account account1 = new Account(0L, "Konto osobiste", "872474992394723984792", BigDecimal.valueOf(9000));
    final Account account2 = new Account(1L, "Konto firmowe", "23489576342809573482", BigDecimal.valueOf(2000));
    final Transaction transaction1 = new Transaction(0L, BigDecimal.valueOf(100), "pozyczka", LOSS);
    final Transaction transaction2 = new Transaction(1L, BigDecimal.valueOf(3000), "oddaje dzieki", INCOME);
    final Transaction transaction3 = new Transaction(2L, BigDecimal.valueOf(200), "splata raty", LOSS);
    user1.addAccount(account1);
    user1.addAccount(account2);
    account1.setUser(user1);
    account2.setUser(user1);
    account1.addTransactionMutually(transaction1);
    account1.addTransactionMutually(transaction2);
    account1.addTransactionMutually(transaction3);

    accounts.addAll(asList(account1, account2));
    users.add(user1);
    transactions.addAll(asList(transaction1, transaction2, transaction3));
  }

  @Inject
  public AccountService(final AccountJpaDao accountJpaDao) {
    this.accountJpaDao = accountJpaDao;
  }

  /*dummy constructor to avoid nonproxyable problem*/
  protected AccountService() {
    this(null);
  }

  public Account findAccount(final long id) {
    return accounts.get((int) id);
  }

  public void editAccount(final Account account) {
    final int accIndex = accounts.indexOf(account);
    if (accIndex == -1) {
      createAccount(account);
    } else {
      final Account editedAccount = accounts.get(accIndex);
      editedAccount.setName(account.getName());
    }
  }

  private void createAccount(final Account account) {
    account.setId((long) accounts.size());
    account.setBalance(BigDecimal.ZERO);
    account.setUser(users.get(0));
    account.setNumber("randomnumber");
    account.setTransactions(new ArrayList<>());
    accounts.add(account);
    users.get(0).addAccount(account);
  }

  public List<Account> getAllAccounts() {
    return accounts;
  }

  /*Move to TransactionService after adding db*/

  public Transaction findTransaction(final long id) {
    return transactions.get((int) id);
  }

  public void transferMoney(final Transaction transaction) {
    final Account account = accounts.get(accounts.indexOf(transaction.getAccount()));
    final BigDecimal resultBalance = account.getBalance().subtract(transaction.getValue());
    if (resultBalance.compareTo(BigDecimal.ZERO) >= 0) {
      transactions.add(transaction);
      account.setBalance(resultBalance);
      account.addTransaction(transaction);
    }
  }

  public Collection<Transaction> getTransactions(final Long accountId) {
    return transactions.stream()
        .filter(t -> t.getAccount().getId().equals(accountId))
        .collect(Collectors.toList());
  }

  public void deleteTransaction(final Transaction transaction) {
    final Optional<Account> account = accounts.stream()
        .filter(acc -> acc.getTransactions().contains(transaction))
        .findFirst();
    account.ifPresent(acc -> acc.getTransactions().remove(transaction));
    transactions.remove(transaction);
  }

  /*Move to UserService after adding db*/
  public User findUser(final long id) {
    return users.get((int) id);
  }

  public void editUser(final User user) {
    final User editedUser = users.get(users.indexOf(user));
    editedUser.setFirstName(user.getFirstName());
    editedUser.setLastName(user.getLastName());
    editedUser.setEmail(user.getEmail());
    editedUser.setMobileNumber(user.getMobileNumber());
  }
}
