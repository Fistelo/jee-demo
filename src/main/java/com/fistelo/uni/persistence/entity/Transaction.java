package com.fistelo.uni.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@EqualsAndHashCode(exclude = "account")
@Getter @Setter
@NoArgsConstructor
@Table(name = "transactions")
public class Transaction implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private BigDecimal value;

  private String receiverAccountNumber;

  private String title;

  @Enumerated(value = EnumType.STRING)
  private Action action;

  @ManyToOne
  private Account account;

  public Transaction(final Long id, final BigDecimal value, final String title, final Action action) {
    this.id = id;
    this.title = title;
    this.value = value;
    this.action = action;
  }
}
