package com.fistelo.uni.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@EqualsAndHashCode(exclude = "user")
@NoArgsConstructor
@Getter @Setter
@Table(name = "accounts")
public class Account implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  private String number;

  private BigDecimal balance;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
  private List<Transaction> transactions = new ArrayList<>();

  @ManyToOne
  private User user;

  public Account(final Long id, final String name, final String number, final BigDecimal balance) {
    this.id = id;
    this.name = name;
    this.number = number;
    this.balance = balance;
  }

  public void addTransactionMutually(final Transaction transaction){
    addTransaction(transaction);
    transaction.setAccount(this);
  }
  public void addTransaction(final Transaction transaction){
    this.transactions.add(transaction);
  }
}
