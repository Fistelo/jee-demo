package com.fistelo.uni.persistence.entity;

public enum Action {
  INCOME, LOSS
}
