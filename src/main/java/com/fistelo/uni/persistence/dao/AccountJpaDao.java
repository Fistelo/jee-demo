package com.fistelo.uni.persistence.dao;

import javax.enterprise.context.ApplicationScoped;

import com.fistelo.uni.persistence.entity.Account;

@ApplicationScoped
public class AccountJpaDao extends JpaDao<Account> {
  public AccountJpaDao() {
    super(Account.class);
  }
}
