package com.fistelo.uni.persistence.dao;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

public abstract class JpaDao<T extends Serializable> implements Dao<T> {

  @PersistenceContext(name = "default")
  EntityManager entityManager;
  @Resource
  UserTransaction transaction;
  private final Class<T> persistentClass;

  public JpaDao(final Class<T> persistentClass) {
    this.persistentClass = persistentClass;
  }

  public T add(final T entity) {
    if (nonNull(entity)) {
      try {
        transaction.begin();
        entityManager.persist(entity);
        transaction.commit();
      } catch (Exception e) {
        try {
          transaction.rollback();
        } catch (SystemException e1) {
          e1.printStackTrace();
        }
        e.printStackTrace();
      }
    }
    return entity;
  }

  public T update(final T entity) {
    if (isNull(entity)) {
      return null;
    }
    return entityManager.merge(entity);
  }

  public List<T> findAll() {
    final String queryString = "SELECT t FROM " + persistentClass.getSimpleName() + " t";
    try {
      transaction.begin();
      final List<T> aaa = entityManager.createQuery(queryString).getResultList();
      transaction.commit();
      return aaa;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public void remove(final T entity) {
    if (nonNull(entity)) {
      entityManager.remove(entity);
    }
  }

  public T find(final String id) {
    if (isNull(id)) {
      return null;
    }
    return entityManager.find(persistentClass, id);
  }

}