package com.fistelo.uni.persistence.dao;

import javax.enterprise.context.ApplicationScoped;

import com.fistelo.uni.persistence.entity.Transaction;

@ApplicationScoped
public class HistoryJpaDao extends JpaDao<Transaction> {
  public HistoryJpaDao(){
    super(Transaction.class);
  }
}
